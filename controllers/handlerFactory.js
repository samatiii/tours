const catchAsync = require('./../utils/catchAsync');
const AppError = require('./../utils/AppError');
const APIFeatures = require('./../utils/apiFeatures');

exports.deleteOne = (Model) =>
  catchAsync(async (req, resp, next) => {
    const doc = await Model.findByIdAndDelete(req.params.id);

    if (!doc) {
      return next(new AppError(`No document found with that ID`, 404));
    }

    resp.status(200).json({
      status: 'success',
    });
  });

exports.updateOne = (Model) =>
  catchAsync(async (req, resp, next) => {
    const document = await Model.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    if (!document) {
      return next(new AppError('No document found with that ID', 404));
    }
    resp.status(200).json({
      status: 'success',
      data: { document },
    });
  });

exports.creatOne = (Model) =>
  catchAsync(async (req, resp, next) => {
    const doc = await Model.create(req.body);

    resp.status(201).json({
      status: 'success',
      data: { doc },
    });
  });

exports.getOne = (Model, popOptions) =>
  catchAsync(async (req, resp, next) => {
    let query = Model.findById(req.params.id);
    if (popOptions) query = query.populate(popOptions);

    const doc = await query;
    if (!doc) {
      return next(new AppError('No document found with that ID', 404));
    }
    resp.status(200).json({
      status: 'success',
      data: { doc },
    });
  });

exports.getAll = (Model) =>
  catchAsync(async (req, resp, next) => {
    let filter = {};
    //check for nasted methode /:id/reviews
    if (req.params.tourId) filter = { tour: req.params.tourId };
    const features = new APIFeatures(
      Model.find(filter).populate('reviews'),
      req.query
    )
      .filter()
      .sort()
      .limitFields()
      .pagination();

    const docs = await features.query;

    resp.status(200).json({
      status: 'success',
      result: docs.length,
      data: docs,
    });
  });
