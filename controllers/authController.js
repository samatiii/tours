const jwt = require('jsonwebtoken');
const User = require('./../models/userModel');
const catchAsync = require('./../utils/catchAsync');
const sendEmail = require('./../utils/email');
const AppError = require('./../utils/appError');
const { promisify } = require('util');
const crypto = require('crypto');
const bcrypt = require('bcryptjs');

const signeToken = (id) => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });
};

const creatSentToken = (user, statusCode, res) => {
  const token = signeToken(user._id);

  const cookieOptions = {
    expires: new Date(
      Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000
    ),
    secure: true, //the cookie will only be send on ecrypted connection
    httpOnly: true, //cookie cant be  modefied on the browser
  };

  if (process.env.NODE_ENV === 'production') {
    cookieOptions.secure = true;
  }

  res.cookie('jwt', token, { cookieOptions });

  user.password = undefined;
  res.status(statusCode).json({
    status: 'success',
    token,
    data: {
      user: user,
    },
  });
};

exports.signup = catchAsync(async (req, res, next) => {
  const checkuser = await User.findOne({ email: req.body.email });
  if (checkuser) {
    return next(
      new AppError(`User with email : ${req.body.email} already existe`, 400)
    );
  }

  const newUSer = await User.create(req.body);

  creatSentToken(newUSer, 201, res);
});

exports.login = catchAsync(async (req, res, next) => {
  const { email, password } = req.body;

  const user = await User.findOne({ email }).select('+password');

  if (!user || !(await user.correctPassword(password, user.password))) {
    return next(new AppError('Incorrect email or password', 401));
  }

  creatSentToken(user, 200, res);
});

// signe in implementaion
exports.protect = catchAsync(async (req, res, next) => {
  let token;
  //1 getting token and check is it's there
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith('Bearer')
  ) {
    token = req.headers.authorization.split(' ')[1];
  }

  if (!token) {
    return next(
      new AppError('you are not loged in ! please login to get acess', 401)
    );
  }
  //2 verification token
  const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);
  //3check is user still exists
  const freshUser = await User.findById(decoded.id);
  if (!freshUser) {
    return next(
      new AppError('The toker belong to a user no longer existe ', 401)
    );
  }
  //4 check is user changed password after toker was issued
  if (freshUser.changedPasswordAfter(decoded.iat)) {
    return next(
      new AppError('USer recently changed password! pleas login again ', 401)
    );
  }

  // Grant acess to protected rout
  req.user = freshUser;
  next();
});

// request Authorisation
exports.restrictTo = (...roles) => {
  return (req, res, next) => {
    if (!roles.includes(req.user.role)) {
      return next(
        new AppError('you do not have permission to perform this action', 403)
      );
    }
    next();
  };
};

exports.forgetPassword = catchAsync(async (req, res, next) => {
  //1 get user based on postes email
  const user = await User.findOne({ email: req.body.email });
  if (!user) {
    return next(new AppError(' there is no user with this email adress ', 404));
  }
  //2 generate the random reset token
  const resetToken = user.creatPasswordResetToken();
  await user.save({ validateBeforeSave: false });

  //send it to user's email
  const resetUrl = `${req.protocol}://${req.get(
    'host'
  )}/api/v1/users/resetPassword/${resetToken}`;

  const message = `Forgot your password? submite a putch request with your new new password and paswordConfir ro : ${resetUrl}.\nIf you didn't forget your password, please ignore this email!`;
  try {
    await sendEmail({
      email: user.email,
      subject: 'your password reset token (valid for 10 min)',
      message,
    });

    res.status(200).json({
      status: 'success',
      message: 'Token sent to email',
    });
  } catch (err) {
    console.log(err);
    user.passwordResetToken = undefined;
    user.passwordResetExpires = undefined;
    await user.save({ validateBeforeSave: false });
    next(
      new AppError('there was an error sending the email, try again later', 500)
    );
  }
});

exports.resetPassword = catchAsync(async (req, res, next) => {
  //1 get user based on token
  const hashedToken = crypto
    .createHash('sha256')
    .update(req.params.token)
    .digest('hex');

  const user = await User.findOne({
    passwordResetToken: hashedToken,
    passwordResetExpires: {
      $gt: Date.now(),
    },
  });

  //2 if token not expired and user existe set new password
  if (!user) {
    return next(new AppError('token is invalide or exprired', 400));
  }
  user.password = req.body.password;
  user.passwordConfirm = req.body.passwordConfirm;
  user.passwordResetToken = undefined;
  user.passwordResetExpires = undefined;
  await user.save();
  //3 update changedpasswordat
  //4log the user in send jwt
  creatSentToken(user, 200, res);
});

exports.updatePassword = catchAsync(async (req, res, next) => {
  //1 get user from collection
  const user = await User.findById(req.user.id).select('+password');
  //check if posted password is correct
  if (!(await user.correctPassword(req.body.passwordCurrent, user.password))) {
    return next(new AppError('Your current password is incorrect'));
  }
  //3 if passworct then update password
  user.password = req.body.password;
  user.passwordConfirm = req.body.passwordConfirm;
  await user.save();
  //4 lo user in with new password
  creatSentToken(user, 200, res);
});
