const AppError = require('./../utils/appError');

const handleCastError = (err) => {
  const message = `Invalid ${err.path} : ${err.value}`;
  return new AppError(message, 400);
};

const handleDuplicateFieldse = (err) => {
  const message = `Duplicate field value `;
  return new AppError(message, 400);
};

const sendErrorDev = (err, resp) => {
  resp.status(err.statusCode).json({
    status: err.status,
    error: err,
    message: err.message,
    stack: err.stack,
  });
};

const sendErrorProd = (err, resp) => {
  if (err.isOperational) {
    resp.status(err.statusCode).json({
      status: err.status,
      message: err.message,
    });
  } else {
    console.log('ERROR : ', err);
    resp.status(500).json({
      status: 'ERROR',
      message: 'Something went wrong',
    });
  }
};

module.exports = (err, req, resp, next) => {
  err.statusCode = err.statusCode || 500;
  err.status = err.status || 'error';
  if (process.env.NODE_ENV === 'developement') {
    sendErrorDev(err, resp);
  } else if (process.env.NODE_ENV === 'production') {
    let error = { ...err };
    if (error.name === 'CastError') {
      error = handleCastError(error);
    }
    if (error.code === 11000) {
      error = handleDuplicateFieldse();
    }
    if (error.name === 'TokenExpiredError') {
      error = handleKWTExpiredError();
    }
    sendErrorProd(error, resp);
  }
};

const handleKWTError = () =>
  new AppError('Invalide token please login again ', 401);

const handleKWTExpiredError = () =>
  new AppError('Your token has expired ! please login again', 401);
