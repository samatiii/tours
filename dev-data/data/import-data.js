const fs = require('fs');
const mongoose = require('mongoose');
const Tour = require('./../../models/tourModel');
const dotenv = require('dotenv');

dotenv.config({ path: './config.env' });

const DB = process.env.DATABASE.replace(
  '<PASSWORD>',
  process.env.DATABASE_PASSWORD
);

mongoose
  .connect(DB, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
  })
  .then(() => console.log('concceting to mongodb for importing data is OK'));

const tours = JSON.parse(fs.readFileSync(`${__dirname}/tours.json`, 'UTF-8'));

const deleteData = async () => {
  try {
    await Tour.deleteMany();
    console.log('Data Deleted');
  } catch (err) {
    console.log(err);
  }
};

const importData = async () => {
  try {
    await Tour.create(tours);
    console.log('Data Imported');
  } catch (err) {
    console.log(err);
  }
};

if (process.argv[2] === '--import') {
  console.log('call Import');
  importData();
} else if (process.argv[2] === '--delete') {
  console.log('call delete');
  deleteData();
}
