class APIFeatures {
  constructor(query, querString) {
    this.query = query;
    this.querString = querString;
  }

  filter() {
    const queryObject = { ...this.querString };
    const excludedFields = ['page', 'sort', 'limit', 'fields'];
    excludedFields.forEach((el) => delete queryObject[el]);

    let queryStr = JSON.stringify(queryObject);
    // /b to match the exact same filter /g to serch match for all params not stoping in fist element
    queryStr = queryStr.replace(/\b(gte|lte|gt|lt)\b/g, (match) => `$${match}`);

    this.query = this.query.find(JSON.parse(queryStr));

    return this;
  }

  sort() {
    if (this.querString.sort) {
      this.query = this.query.sort(this.querString.sort);
    } else {
      this.query = this.query.sort('-createdAt');
    }
    return this;
  }

  limitFields() {
    if (this.querString.fields) {
      let fields = this.querString.fields.split(',').join(' ');
      console.log(fields);
      this.query = this.query.select(fields);
    } else {
      this.query = this.query.select('-__v');
    }
    return this;
  }

  pagination() {
    const page = this.querString.page * 1 || 1; // || 1 is a js default value
    const limit = this.querString.limit * 1 || 100;
    const skip = (page - 1) * limit;
    this.query = this.query.skip(page).limit(limit);

    return this;
  }
}

module.exports = APIFeatures;
