const nodemailer = require('nodemailer');

const sendEmail = async (options) => {
  //1 creat a transporter
  const transporter = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    auth: {
      user: process.env.EMAIL_USERNAME,
      pass: process.env.EMAIL_PASSWORD,
    },
  });
  //2 define email options
  const mailoptions = {
    from: 'abdessamad abdell <abdell@gmail.com>',
    to: options.email,
    subject: options.subject,
    text: options.message,
  };

  //3 send the email with noce mailer
  await transporter.sendMail(mailoptions);
};
module.exports = sendEmail;
