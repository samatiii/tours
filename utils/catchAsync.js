module.exports = (fn) => {
  //this is the fuction that express gonna call
  return (req, res, next) => {
    fn(req, res, next).catch(next);
  };
};
