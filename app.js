const express = require('express');
const tourRout = require('./routes/tourRoutes');
const userRout = require('./routes/userRoutes');
const reviewRout = require('./routes/reviewRoutes');
const rateLimit = require('express-rate-limit');
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');
const hpp = require('hpp');
const AppError = require('./utils/appError');
const globalError = require('./controllers/errorContoller');
const app = express();

// Globale Midlerwares
// security http request
app.use(helmet());

//body parser reading data from body to raq.body
app.use(express.json());

//Data sanitization against NoSql query injection
app.use(mongoSanitize());

//Data sanitization against XSS : clean any user inpute from any html/scripte attahcted to it (converte theme )
app.use(xss());

// prevente parameter polution (?sort=duration$sort=price) butallow duplicate like duration
app.use(
  hpp({
    whitelist: [
      'duration',
      'ratingsQuantity',
      'ratingAverage',
      'maxGroupeSize',
      'difficulty',
      'price',
    ],
  })
);

// Serving static files
app.use(express.static(`${__dirname}/public`));

// time midelware
app.use((req, resp, next) => {
  req.requestTime = new Date().toISOString();
  next();
});

const limiter = rateLimit({
  max: 90,
  windowMs: 60 * 60 * 1000,
  message: 'Too many request from this IP, please try again in an hour',
});

app.use('/api', limiter);

app.use('/api/v1/tours', tourRout);
app.use('/api/v1/users', userRout);
app.use('/api/v1/reviews', reviewRout);

app.all('*', (req, resp, next) => {
  next(new AppError(`Can't find ${req.originalUrl} on this server`, 404));
});

app.use(globalError);

module.exports = app;
