const mongoose = require('mongoose');
//const user = require('./userModel');

const tourScema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'A toure must have a name'],
      trim: true,
    },
    duration: {
      type: Number,
      required: [true, 'A tour must have a ducation'],
    },
    maxGroupSize: {
      type: Number,
      required: [true, 'A tour must have a groupe size'],
    },
    difficulty: {
      type: String,
      required: [true, 'A tour must have a  difficulty'],
    },
    ratingsAverage: {
      type: Number,
      default: 4.7,
    },
    ratingsQuantity: {
      type: Number,
      default: 0,
    },
    price: {
      type: Number,
      required: [true, 'A tour must have a price'],
    },
    priceDiscount: Number,
    summary: {
      type: String,
      trim: true,
    },
    description: {
      type: String,
      trim: true,
      required: [true, 'A tour must have a description'],
    },
    imageCover: {
      type: String,
      required: [true, 'A tour must have a image'],
    },
    images: [String],
    createdAt: {
      type: Date,
      default: Date.now(),
    },
    startDates: [Date],
    startLocation: {
      type: {
        type: String,
        default: 'Point',
        enum: ['Point'],
      },
      coordinates: [Number],
      address: String,
      description: String,
    },
    locations: [
      {
        type: {
          type: String,
          default: 'Point',
          enum: ['Point'],
        },
        coordinates: [Number],
        address: String,
        description: String,
        day: Number,
      },
    ],
    guides: [
      {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
      },
    ],
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

//virtual populating for getting review for a tour
tourScema.virtual('reviews', {
  ref: 'Review',
  foreignField: 'tour',
  localField: '_id',
});

// to save user when embeding user into tours
// tourScema.pre('save', async function (next) {
//   const guidesPromesses = this.guides.map(
//     async (id) => await user.findById(id)
//   );
//   this.guides = await Promise.all(guidesPromesses);
//   next();
// });

// a midlware to populate data of user : second query to get data about user (using a reference in the tour model)
tourScema.pre(/^find/, function (next) {
  this.populate({
    path: 'guides',
    select: '-__v -passwordChangedAt',
  });
  next();
});

const Tour = mongoose.model('Tour', tourScema);

module.exports = Tour;
