const mongoose = require('mongoose');

const reviewScema = new mongoose.Schema({
  review: {
    type: String,
    require: [true, 'Review can not be empty'],
  },
  rating: {
    type: Number,
    min: 1,
    max: 5,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  tour: {
    type: mongoose.Schema.ObjectId,
    ref: 'Tour',
    require: [true, 'Review must belong to a Tour'],
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    require: [true, 'Review must belong to a user'],
  },
});

reviewScema.pre(/^find/, function (next) {
  this.populate({
    path: 'user',
    select: 'name email',
  });
  // this.populate({
  //   path: 'tour',
  //   select: 'name',
  // }).populate({
  //   path: 'user',
  //   select: 'name email',
  // });

  next();
});

const Review = mongoose.model('Review', reviewScema);

module.exports = Review;
