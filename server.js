const dotenv = require('dotenv');
const mongoose = require('mongoose');
dotenv.config({ path: './config.env' });
const app = require('./app');

// shut down is not option the node process is an unclean state
process.on('uncaughtException', (err) => {
  console.log('Uncaught exception ! Shutting Down...  : ', err.name);
});

const DB = process.env.DATABASE.replace(
  '<PASSWORD>',
  process.env.DATABASE_PASSWORD
);
//mangoose connection
mongoose
  .connect(DB, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then((connection) => {
    console.log('MangoDB connected successfuly..');
  });

// start listnening
const port = process.env.PORT || 3000;

const server = app.listen(port, () => {
  console.log(`App running in port ${port}`);
});

// for database exception for exemple
// shut down application is optional
process.on('unhandledRejection', (err) => {
  console.log(err.name);
  server.close(() => {
    process.exit(1);
  });
});
